<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as JMS;

/**
 * Tarea
 *
 * @ORM\Table(name="tarea")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TareaRepository")
 * @UniqueEntity("nombre", message="Ya existe una tarea con ese nombre")
 */
class Tarea
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="No se puede dejar el nombre de la tarea vacío")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="prioridad", type="integer")
     * @Assert\NotBlank(message="No se puede dejar el nombre de la tarea vacío")
     * @Assert\Range(
     *      min = 1,
     *      max = 3,
     *      invalidMessage = "La prioridad va de 1 a 3",
     *      minMessage = "La prioridad va de 1 a 3",
     *      maxMessage = "La prioridad va de 1 a 3"
     * )
     */
    private $prioridad;

    /**
     * @var bool
     *
     * @ORM\Column(name="estado", type="string", length=255)
     * @Assert\Choice(
     *     choices = {"TERMINADA", "PENDIENTE"},
     *     message = "El estado debe de ser TERMINADA o PENDIENTE"
     * )
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_alta", type="datetime")
     * @Assert\NotBlank(message="No se puede dejar vacía la fecha de alta")
     * @JMS\Type("DateTime")
     * @JMS\SerializedName("fechaAlta")
     */
    private $fechaAlta;

    public function __construct()
    {
        $this->fechaAlta = new \DateTime('now');
        $this->prioridad = 1;
        $this->estado = 'PENDIENTE';
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setPrioridad($prioridad)
    {
        $this->prioridad = $prioridad;

        return $this;
    }

    public function getPrioridad()
    {
        return $this->prioridad;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }
}