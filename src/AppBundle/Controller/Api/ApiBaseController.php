<?php


namespace AppBundle\Controller\Api;

use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ApiBaseController extends Controller
{
    private function respuesta($result, $statusCode, $serialization_group=null)
    {
        if ($serialization_group !== null)
        {
            $context = SerializationContext::create()->setGroups(array($serialization_group));
            $json_result = $this->get('jms_serializer')->serialize($result, "json", $context);
        }
        else
            $json_result = $this->get('jms_serializer')->serialize($result, "json");

        return new Response($json_result, $statusCode, array(
            'Content-Type' => 'application/json'
        ));
    }

    protected function respuestaCorrecta($data, $statusCode=Response::HTTP_OK, $serialization_group=null)
    {
        $result['code'] = $statusCode;
        $result['data'] = $data;

        return $this->respuesta($result, $statusCode, $serialization_group);
    }

    private function respuestaConErrores(array $errores, $statusCode=Response::HTTP_NOT_FOUND)
    {
        $result['code'] = $statusCode;
        $result['messages'] = $errores;

        return $this->respuesta($result, $statusCode);
    }

    protected function respuestaUsuarioNoAutorizado()
    {
        $errores[] = "Usuario no autorizado, puede haber expirado el token";
        return $this->respuestaConErrores($errores, Response::HTTP_UNAUTHORIZED);
    }

    protected function respuestaNoEncontrado($nombre_entidad, $id)
    {
        $errores[] = "No existe ningún registro de $nombre_entidad con el id $id";
        return $this->respuestaConErrores($errores);
    }

    protected function respuestaDatosNoValidos(array $errores = null)
    {
        if ($errores === null)
            $errores = array("Datos no válidos");

        return $this->respuestaConErrores($errores, Response::HTTP_BAD_REQUEST);
    }

    protected function guarda($entity)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
    }

    private function getErroresValidacion($entity)
    {
        $validator = $this->get('validator');
        $validateErrors = $validator->validate($entity);
        $errores = array();

        foreach ($validateErrors as $validateError)
            $errores[] = $validateError->getMessage();

        return $errores;
    }

    protected function procesaDatos($entity, $statusCode=Response::HTTP_OK, $serialization_group=null)
    {
        $errores = $this->getErroresValidacion($entity);
        if (count($errores) === 0)
        {
            $this->guarda($entity);

            return $this->respuestaCorrecta($entity, $statusCode, $serialization_group);
        }
        else
            return $this->respuestaDatosNoValidos($errores);
    }

    protected function eliminaEntidad($entity)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return $this->respuestaCorrecta(null, Response::HTTP_NO_CONTENT);
    }
}