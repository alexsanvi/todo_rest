<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Tarea;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class TareaController extends ApiBaseController
{
    /**
     * @Route("/api/tareas")
     * @Route("/api/tareas/ordenadas/{orden}")
     * @Method("GET")
     * @ApiDoc(
     *  section="Tareas",
     *  resource=true,
     *  description="Devuelve una colección de tareas. Los parámetros se pasan por query string.",
     *  requirements={
     *      {
     *          "name"="orden",
     *          "dataType"="string",
     *          "description"="nombre del campo por el que se desea ordenar (nombre|estado|prioridad|fechaAlta)"
     *      }
     *  },
     *  filters={
     *      {"name"="nombre", "dataType"="string", "description"="busca porciones de nombres, no es necesario introducir el nombre completo"},
     *      {"name"="prioridad", "dataType"="integer", "description"="Va de 1 a 3"},
     *      {"name"="estado", "dataType"="string", "pattern"="(TERMINADA|PENDIENTE)"}
     *  },
     *  statusCodes={
     *         200="Resultado OK",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function listAction(Request $request, $orden=null)
    {
        $nombre = $request->query->get('nombre');
        $prioridad = $request->query->get('prioridad');
        $estado = $request->query->get('estado');

        $tareas = $this->getDoctrine()
            ->getRepository('AppBundle:Tarea')
            ->findAllWithFilter($nombre, $prioridad, $estado, $orden);

        return $this->respuestaCorrecta($tareas);
    }

    /**
     * @Route("/api/tareas")
     * @Method("POST")
     * @ApiDoc(
     *  section="Tareas",
     *  description="Crea una nueva tarea. Los parámetros se pasan por JSon.",
     *  parameters={
     *      {
     *          "name"="nombre",
     *          "dataType"="string",
     *          "required"=true,
     *          "description"="Nombre de la tarea. Debe ser único."
     *      },
     *      {
     *          "name"="prioridad",
     *          "dataType"="integer",
     *          "required"=false,
     *          "description"="Prioridad de la tarea de 1 a 3. Si no se indica se le asigna el valor 1"
     *      },
     *      {
     *          "name"="estado",
     *          "dataType"="string",
     *          "required"=false,
     *          "description"="Estado de la tarea (TERMINADA|PENDIENTE). Si no se indica se le asigna el valor PENDIENTE"
     *      },
     *      {
     *          "name"="fechaAlta",
     *          "dataType"="datetime",
     *          "required"=false,
     *          "description"="Fecha de alta de la tarea. Si no se indica tomará la fecha actual"
     *      }
     *  },
     *  statusCodes={
     *         201="Tarea creada",
     *         400="Datos no válidos",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function newAction(Request $request)
    {
        $tarea = $this->get('jms_serializer')->deserialize($request->getContent(), Tarea::class, "json");

        return $this->procesaDatos($tarea, Response::HTTP_CREATED);
    }

    /**
     * @Route("/api/tareas/{id}", requirements={"id": "\d+"})
     * @Method("GET")
     * @ApiDoc(
     *  section="Tareas",
     *  description="Muestra una tarea",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id de la tarea a mostrar"
     *      }
     *  },
     *  statusCodes={
     *         200="Resultado OK",
     *         400="Datos no válidos",
     *         404="Tarea no encontrada",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function showAction(Request $request, $id)
    {
        $tarea = $this->getDoctrine()->getRepository('AppBundle:Tarea')->find($id);

        if ($tarea)
        {
            return $this->respuestaCorrecta($tarea);
        }
        else
            return $this->respuestaNoEncontrado("tarea", $id);
    }

    /**
     * @Route("/api/tareas/{id}", requirements={"id": "\d+"})
     * @Method("PUT")
     * @ApiDoc(
     *  section="Tareas",
     *  description="Modifica una tarea. Los parámetros se pasan por Json.",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id de la tarea a modificar"
     *      }
     *  },
     *  parameters={
     *      {
     *          "name"="nombre",
     *          "dataType"="string",
     *          "required"=false,
     *          "description"="Nombre de la tarea. Debe ser único."
     *      },
     *      {
     *          "name"="prioridad",
     *          "dataType"="integer",
     *          "required"=false,
     *          "description"="Prioridad de la tarea de 1 a 3. Si no se indica se le asigna el valor 1"
     *      },
     *      {
     *          "name"="estado",
     *          "dataType"="string",
     *          "required"=false,
     *          "description"="Estado de la tarea (TERMINADA|PENDIENTE). Si no se indica se le asigna el valor PENDIENTE"
     *      },
     *      {
     *          "name"="fechaAlta",
     *          "dataType"="datetime",
     *          "required"=false,
     *          "description"="Fecha de alta de la tarea. Si no se indica tomará la fecha actual"
     *      }
     *  },
     *  statusCodes={
     *         200="Resultado OK",
     *         400="Datos no válidos",
     *         404="Tarea no encontrada",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function updateAction(Request $request, $id)
    {
        try
        {
            $data = json_decode($request->getContent(), true);
            $data['id'] = $id;
            $tarea = $this->get('jms_serializer')->deserialize(json_encode($data), Tarea::class, "json");
            return $this->procesaDatos($tarea);
        }
        catch(\Exception $exception)
        {
            return $this->respuestaNoEncontrado("tarea", $id);
        }
    }

    /**
     * @Route("/api/tareas/{id}", requirements={"id": "\d+"})
     * @Method("DELETE")
     * @ApiDoc(
     *  section="Tareas",
     *  description="Elimina una tarea. Los parámetros se pasan por Json.",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id de la tarea a eliminar"
     *      }
     *  },
     *  statusCodes={
     *         204="Resultado OK",
     *         404="Tarea no encontrada",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function deleteAction(Request $request, $id)
    {
        $tarea = $this->getDoctrine()->getRepository('AppBundle:Tarea')->find($id);

        if ($tarea)
            return $this->eliminaEntidad($tarea);
        else
            return $this->respuestaNoEncontrado("tarea", $id);
    }
}
