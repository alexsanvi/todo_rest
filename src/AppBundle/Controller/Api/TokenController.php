<?php

namespace AppBundle\Controller\Api;

use AppBundle\Controller\Api\ApiBaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class TokenController extends ApiBaseController
{
    /**
     * @Route("/api/tokens")
     * @Method("POST")
     * @ApiDoc(
     *  section="Tokens",
     *  description="Obtiene un token de seguridad a partir de un usuario y un password. Los parámetros username y password se pasan por POST.",
     *  parameters={
     *      {
     *          "name"="username",
     *          "dataType"="string",
     *          "required"=true,
     *          "description"="Nombre de usuario"
     *      },
     *      {
     *          "name"="password",
     *          "dataType"="string",
     *          "required"=true,
     *          "description"="Password del usuario"
     *      }
     *  },
     *  statusCodes={
     *         200="Token Creado OK",
     *         401="Usuario no autorizado"
     *  }
     * )
     */
    public function newTokenAction(Request $request)
    {
        return $this->respuestaCorrecta("", Response::HTTP_UNAUTHORIZED);
    }
}
