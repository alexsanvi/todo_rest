<?php


namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class UserController extends ApiBaseController
{
    private function encriptaPassword(User $usuario)
    {
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($usuario, $usuario->getPassword());

        $usuario->setPassword($encoded);
    }

    /**
     * @Route("/api/usuarios")
     * @Method("GET")
     * @ApiDoc(
     *  section="Usuarios",
     *  resource=true,
     *  description="Devuelve una colección de usuarios.",
     *  statusCodes={
     *         200="Resultado OK",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function listAction(Request $request)
    {
        $usuarios = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();

        return $this->respuestaCorrecta($usuarios, Response::HTTP_OK, "sinpassword");
    }

    /**
     * @Route("/api/usuarios")
     * @Method("POST")
     * @ApiDoc(
     *  section="Usuarios",
     *  description="Crea una nuevo usuario. Los parámetros se pasan por Json.",
     *  parameters={
     *      {
     *          "name"="username",
     *          "dataType"="string",
     *          "required"=true,
     *          "description"="Nombre de usuario. Debe ser único."
     *      },
     *      {
     *          "name"="password",
     *          "dataType"="string",
     *          "required"=true,
     *          "description"="Password del usuario"
     *      }
     *  },
     *  statusCodes={
     *         201="Usuario creado",
     *         400="Datos no válidos",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function newAction(Request $request)
    {
        $usuario = $this->get('jms_serializer')->deserialize($request->getContent(), User::class, "json");

        if ($usuario->getPassword() === null || $usuario->getPassword() === '')
        {
            $errores[] = "No se puede dejar el password vacío";
            return $this->respuestaDatosNoValidos($errores);
        }
        $this->encriptaPassword($usuario);

        $usuario->setAvatar(
            $request->getUriForPath($this->getParameter('url_avatars_directory').'default.png'));

        return $this->procesaDatos($usuario, Response::HTTP_CREATED, "sinpassword");
    }

    /**
     * @Route("/api/usuarios/{id}", requirements={"id": "\d+"})
     * @Method("GET")
     * @ApiDoc(
     *  section="Usuarios",
     *  description="Muestra un usuario",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id del usuario a mostrar"
     *      }
     *  },
     *  statusCodes={
     *         200="Resultado OK",
     *         400="Datos no válidos",
     *         404="Usuario no encontrada",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function showAction(Request $request, $id)
    {
        $usuario = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        if ($usuario)
            return $this->respuestaCorrecta($usuario, Response::HTTP_OK, "sinpassword");
        else
            return $this->respuestaNoEncontrado("usuario", $id);
    }

    /**
     * @Route("/api/usuarios/{id}/password", requirements={"id": "\d+"})
     * @Method("PUT")
     * @ApiDoc(
     *  section="Usuarios",
     *  description="Cambia el password de un usuario. Los parámetros se pasan por Json.",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id del usuario a modificar"
     *      }
     *  },
     *  parameters={
     *      {
     *          "name"="password",
     *          "dataType"="string",
     *          "required"=true,
     *          "description"="Password del usuario"
     *      }
     *  },
     *  statusCodes={
     *         200="Resultado OK",
     *         400="Datos no válidos",
     *         404="Usuario no encontrado",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function updatePasswordAction(Request $request, $id)
    {
        $usuario = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        if ($usuario)
        {
            $data = json_decode($request->getContent(), true);

            $usuario->setPassword($data['password']);

            $this->encriptaPassword($usuario);

            return $this->procesaDatos($usuario, Response::HTTP_OK, "sinpassword");
        }
        else
            return $this->respuestaNoEncontrado("usuario", $id);
    }

    /**
     * @Route("/api/usuarios/{id}/avatar", requirements={"id": "\d+"})
     * @Method("PUT")
     * @ApiDoc(
     *  section="Usuarios",
     *  description="Cambia el avatar de un usuario. ",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id del usuario a modificar"
     *      }
     *  },
     *  parameters={
     *      {
     *          "name"="avatar",
     *          "dataType"="string",
     *          "required"=true,
     *          "description"="Avatar del usuario"
     *      }
     *  },
     *  statusCodes={
     *         200="Resultado OK",
     *         400="Datos no válidos",
     *         404="Usuario no encontrado",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function updateAvatarAction(Request $request, $id)
    {
        $usuario = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        if ($usuario)
        {
            $data = json_decode($request->getContent(), true);

            if ($data['avatar'] !== null)
            {
                $arr_avatar = explode(',', $data['avatar']);
                $avatar = base64_decode($arr_avatar[1]);

                if ($avatar)
                {
                    $fileName = $usuario->getUsername().'-'.time().'.jpg';
                    $filePath = $this->getParameter('avatars_directory').$fileName;
                    $urlAvatar = $request->getUriForPath($this->getParameter('url_avatars_directory').$fileName);

                    $usuario->setAvatar($urlAvatar);

                    $ifp = fopen($filePath, "wb");
                    if ($ifp)
                    {
                        $ok = fwrite($ifp, $avatar);
                        if ($ok)
                            return $this->procesaDatos($usuario, Response::HTTP_OK, "sinpassword");

                        fclose($ifp);
                    }
                }
            }

            $errores[] = "No se ha podido cargar la imagen del avatar";
            return $this->respuestaDatosNoValidos($errores);
        }
        else
            return $this->respuestaNoEncontrado("usuario", $id);
    }

    /**
     * @Route("/api/usuarios/{id}", requirements={"id": "\d+"})
     * @Method("DELETE")
     * @ApiDoc(
     *  section="Usuarios",
     *  description="Elimina un usuario. Los parámetros se pasan por Json.",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id del usuario a eliminar"
     *      }
     *  },
     *  statusCodes={
     *         204="Resultado OK",
     *         404="Usuario no encontrada",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function deleteAction(Request $request, $id)
    {
        $usuario = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);

        if ($usuario)
            return $this->eliminaEntidad($usuario);
        else
            return $this->respuestaNoEncontrado("usuario", $id);
    }

    /**
     * @Route("/api/usuarios/profile")
     * @Method("GET")
     * @ApiDoc(
     *  section="Usuarios",
     *  description="Muestra los datos del usuarios actualmente logueado",
     *  statusCodes={
     *         200="Resultado OK",
     *         404="Usuario no encontrado",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function profileAction(Request $request)
    {
        $usuario = $this->get('security.token_storage')->getToken()->getUser();

        if ($usuario)
            return $this->respuestaCorrecta($usuario, Response::HTTP_OK, "sinpassword");

        return $this->respuestaUsuarioNoAutorizado();
    }
}